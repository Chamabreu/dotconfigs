#!/bin/bash

zscroll --delay 0.2 \
  --scroll-padding "      " \
  --length 30 \
  --match-command "playerctl -p spotify status"\
  --match-text "Playing" "--after-text '  '" \
  --match-text "Paused" "--after-text '  '   --scroll 0" \
  --update-check true "playerctl -p spotify metadata --format '{{ artist }} - {{ title }}'" &
wait

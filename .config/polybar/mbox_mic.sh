#!/bin/bash

mic_description="alsa_input.usb-Avid_Avid_Mbox_Mini-00.analog-stereo"

mic_index=$(pactl list sources short | awk -v mic="$mic_description" '$0 ~ mic {print $2}')

# Toggle microphone mute state
pactl set-source-mute "$mic_index" toggle


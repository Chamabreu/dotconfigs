#!/bin/bash

value=-1

file="$HOME/.config/redshift/redshift.conf"
while read line; do
    if [[ "$line" == brightness* ]]; then
        # Zeile gefunden, Wert ändern
        value=$(echo "$line" | awk -F= '{print $2}')
        break
    fi
done < $file

if [[ $value == 1 ]]; then
value=
fi

if [[ $value == 0.6 ]]; then
value=
fi


if [[ $value == 0.3 ]]; then
value=
fi


echo $value

#!/bin/bash

mic_description="alsa_input.usb-Avid_Avid_Mbox_Mini-00.analog-stereo"

mic_index=$(pactl list sources short | awk -v mic="$mic_description" '$0 ~ mic {print $1}')

# Toggle microphone mute state
#pactl set-source-mute "$mic_index" toggle

# Get the current mute state of the microphone
current_state=$(pactl get-source-mute $mic_index | awk '{print $2}')

if [[ $current_state == "yes" ]]; then
  echo 
else
  echo 
fi

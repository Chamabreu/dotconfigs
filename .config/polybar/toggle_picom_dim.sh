#!/bin/bash
#



# Datei öffnen und nach Zeile mit "brightness" suchen
file="$HOME/.config/picom.conf"
while read line; do
    if [[ "$line" == inactive-dim* ]]; then
        value=$(echo "$line" | awk -F= '{print $2}')
        echo $value
        newValue=0
        if [[ $value == 0 ]]; then
          newValue=0.3
        else
          newValue=0
        fi

        sed -i "s/inactive-dim=.*/inactive-dim=$newValue/" $file
        break
    fi
done < $file


#!/bin/bash
#

# Datei öffnen und nach Zeile mit "brightness" suchen
file="~/.config/redshift/redshift.conf"
while read line; do
    if [[ "$line" == brightness* ]]; then
        # Zeile gefunden, Wert ändern
        value=$(echo "$line" | awk -F= '{print $2}')
        newValue=0
        if [[ $value == 1 ]]; then
          newValue=0.6
        elif [[ $value == 0.6 ]]; then
          newValue=0.3
        else
          newValue=1
        fi

        sed -i "s/brightness=.*/brightness=$newValue/" $file
        break
    fi
done < $file

systemctl --user restart redshift

return {
	"folke/trouble.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	opts = {},
	cmd = "Trouble",
	keys = {
		{ "<leader>xx", "<cmd>Trouble<CR>" },
		{ "<leader>xw", "<cmd>Trouble workspace_diagnostics<CR>" },
		{ "<leader>xd", "<cmd>Trouble document_diagnostics<CR>" },
		{ "<leader>xq", "<cmd>Trouble quickfix<CR>" },
		{ "<leader>xl", "<cmd>Trouble loclist<CR>" },
		-- {"n", "gR", "<cmd>TroubleToggle lsp_references<CR>"},
	},
}

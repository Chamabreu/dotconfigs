return {
  "rmagatti/auto-session",
  config = function()
    require("auto-session").setup({
      --log_level = "",
      -- auto_session_suppress_dirs = { "~/", "~/Projects", "~/Downloads", "/" },
      pre_save_cmds = { 'Neotree close' },
      post_restore_cmds = { 'Neotree filesystem position=right show' },
    })
  end,
}

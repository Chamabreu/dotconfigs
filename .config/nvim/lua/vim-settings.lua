vim.cmd("let g:netrw_liststyle = 3")

vim.g.mapleader = " "

local opt = vim.opt
opt.relativenumber = true
opt.number = true

opt.tabstop = 2
opt.shiftwidth = 2
opt.expandtab = true
opt.autoindent = true

opt.wrap = false

opt.ignorecase = true
opt.smartcase = true

opt.cursorline = true

opt.termguicolors = true
opt.background = "dark"
opt.signcolumn = "yes"

opt.backspace = "indent,eol,start"

opt.clipboard:append("unnamedplus")

local keymap = vim.keymap

keymap.set("i", "jj", "<Esc>", { noremap = true })
keymap.set("n", "<leader>nh", ":nohl<CR>", { noremap = true })
-- set leader se to equal panes
keymap.set("n", "<leader>se", ":wincmd =<CR>", { noremap = true })
